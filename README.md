# Posts database
> Posts service database

* [Schema](#schema)
  * [Tables](#tables)
    * [schema_version](#schema_version)
    * [users](#users)
    * [posts](#posts)
* [Charge database](#charge-database)
* [Update database schema](#update-database-schema)
* [Update database functions](#update-database-functions)
* [Update database views](#update-database-views)
* [Update database triggers](#update-database-triggers)
* [Docker image](#docker-image)

## Schema

**Last version**: 1.0.0

### Tables

#### schema_version

| Column name | Column Type | Constraints |
| ----------- | ----------- | ----------- |
| id | SERIAL | PRIMARY KEY |
| major_version | INTEGER | NOT NULL |
| minor_version | INTEGER | NOT NULL |
| build_version | INTEGER | NOT NULL |
| applied | TIMESTAMP WITH TIME ZONE | DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL |

#### users

| Column name | Column Type | Constraints |
| ----------- | ----------- | ----------- |
| id | BIGSERIAL | PRIMARY KEY |
| user_uuid | UUID | DEFAULT uuid_generate_v1mc() NOT NULL UNIQUE-1 |
| added_at | TIMESTAMP WITH TIME ZONE | DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL |

#### posts

| Column name | Column Type | Constraints |
| ----------- | ----------- | ----------- |
| id | BIGSERIAL | PRIMARY KEY |
| post_uuid | UUID | DEFAULT uuid_generate_v1mc() NOT NULL UNIQUE-1 |
| owner | BIGINT | NOT NULL REFERENCES users(id) ON DELETE RESTRICT ON UPDATE RESTRICT |
| title | VARCHAR(25) | NOT NULL |
| created_at | TIMESTAMP WITH TIME ZONE | DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL |

## Charge database

To charge the database you should load the [baseline](baseline.sql) script, then
run the update patches in order. To do this you can use the
[dbcharger](https://gitlab.com/redpanda-utils/dbcharger) utility connect to the
database, create or update to the last version on the repository.

## Update database schema

To update database schema, create a new patch file, with the version name on the
updates directory: *patch-1-0.1.sql*, *patch-1-1-0.sql*, *patch-2-1-4.sql*.

Be sure to include at the end the insert of the database version:

```sql
INSERT INTO {{schema}}.schema_version VALUES(
    DEFAULT,
    major,
    minor,
    build,
    'Description of the patch',
    DEFAULT
);
```

**NEVER** edit one patch file that it's already merged on master branch. This
would cause database inconsistences. Create a new patch instead.

## Update database functions

To update functions or create new ones, just edit the corresponding files or
create a new one. Be sure to include the DROP command at the start of the script
so your function will be reloaded.

## Update database views

To update views or create new ones, just edit the corresponding files or
create a new one. Be sure to include the DROP command at the start of the script
so your view will be reloaded.

## Update database triggers

To update triggers or create new ones, just edit the corresponding files or
create a new one. Be sure to include the DROP command at the start of the script
so your trigger will be reloaded.


## Docker image

To create a docker image run the following command:

```bash
[user@host] $ docker build -t image_name:tag .
```

This image will have one database named *posts* with three schemas: *develop*,
*staging*, *production*.
